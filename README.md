# Setup Python Virtual Environment on Control Node

```bash
# install venv package
sudo apt install python3-venv -y

# create virtual environment
python3 -m venv ansible_venv

# active virtual environment
source ansible_venv/bin/activate

# to disable virtual environment run the command: 
#deactivate

# install ansible-core
pip3 install ansible-core

# install ansible-lint (https://ansible.readthedocs.io/projects/lint/)
pip3 install ansible-lint
```


# TESTING #

```bash
# verify installation
ansible --version

# start one managed node
vagrant up test1

# test ansible ad-hoc command
ansible test -l test1 -m ansible.builtin.ping

# test ansible playbook
ansible-playbook -l test1 first-playbook.yml

# run ansible lint
ansible-lint first-playbook.yml
```


# System Overview Managed nodes

| Hostname  | Environment | OS               |
| --------- | ----------- | ---------------- |
| test1     | test        | Ubuntu 22.04 LTS |
| test2     | test        | AlmaLinux 9      |
| prod1     | prod        | Ubuntu 22.04 LTS |
| prod2     | prod        | AlmaLinux 9      |